from flask import Flask, render_template, request
from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import Required, Email
from flask.ext.mail import Mail
from werkzeug.contrib.fixers import ProxyFix

class ReverseProxied(object):
    '''Wrap the application in this middleware and configure the 
    front-end server to add these headers, to let you quietly bind 
    this to a URL other than / and to an HTTP scheme that is 
    different than what is used locally.

    In nginx:
    location /myprefix {
        proxy_pass http://192.168.0.1:5001;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Scheme $scheme;
        proxy_set_header X-Script-Name /myprefix;
        }

    :param app: the WSGI application
    '''
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '')
        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]

        scheme = environ.get('HTTP_X_SCHEME', '')
        if scheme:
            environ['wsgi.url_scheme'] = scheme
        return self.app(environ, start_response)


app = Flask(__name__)
mail = Mail(app)

app.config['SECRET_KEY'] = 'this is really long and will be changed for production'
app.config['MAIL_SERVER'] = ''
app.config['MAIL_PORT'] = ''
app.config['MAIL_USERNAME'] = ''
app.config['FLASK_MAIL_SUBJECT_PREFIX'] = '[tgraviett]'
app.config['FLASKY_MAIL_SENDER'] = 'Graviett Mailer <mailer@graviett.com>'
app.debug=True
app.wsgi_app = ReverseProxied(app.wsgi_app)


def send_mail(email, name, body, **kwargs):
	msg = Message(app.config['FLASK_MAIL_SUBJECT_PREFIX'] + 'New Message! From:'+
				  name, sender=app.config['FLASK_MAIL_SENDER'], recipients=[to])
	msg.body = 'Email From:' + email + body
	mail.send(msg)

class ContactForm(Form):
	name = StringField('Name:', validators=[Required()])
	email = StringField('Email:', validators=[Required(), Email()])
	body = TextAreaField('Message:', validators=[Required()])
	submit = SubmitField('Submit', validators=[Required()])

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/resume')	
def resume():
	return render_template('resume.html')
	
@app.route('/samples')
def samples():
	return render_template('samples.html')
	
@app.route('/aboutme')
def aboutme():
	return render_template('aboutme.html')

@app.route('/goals')
def goals():
	return render_template('goals.html')

@app.route('/contact', methods=['GET', 'POST'])
def contact():
	form = ContactForm()
	email = None
	message = None
	if request.method == 'POST' and form.validate_on_submit():
		name = form.name.data
		email = form.email.data
		body = form.body.data
		send_mail(email, name, body)
		return redirect(url_for('messagesuccess.html'))
	return render_template('contact.html', form=form, email=email, message=message)
	
@app.route('/messagesuccess')
def messagesuccess():
	return render_tempalate('messagesuccess.html')
	
@app.route('/messagefail')
def messagefail():
	return render_tempalate('messagefail.html')

	
@app.errorhandler(404)	
def page_not_found(e):
	return render_template('404.html'), 404

#@app.errorhandler(500)
#def internal_server_error(e):
#	return render_template('500.html'), 500

app.wsgi_app = ProxyFix(app.wsgi_app)
	
if __name__ == '__main__':
  app.run()
